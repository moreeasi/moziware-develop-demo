
package cn.moziware.developdemo.adaptor;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Adapter used to drive the tile view
 */
public class MainMenuTileAdaptor extends BaseAdapter {
    private final View[] mTileList;

    public MainMenuTileAdaptor(View[] tileList) {
        mTileList = tileList;
    }

    @Override
    public int getCount() {
        if (mTileList == null) {
            return 0;
        }

        return mTileList.length;
    }

    @Override
    public Object getItem(int position) {
        if (mTileList == null || position < 0 || position >= mTileList.length) {
            return null;
        }

        return mTileList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        return mTileList[position];
    }

    public boolean isEnabled(int position) {
        return true;
    }
}
