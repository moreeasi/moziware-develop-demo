package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.TextView;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to get key values about cimo on moziware device
 */
public class KeyValuesAboutCimoActivity extends Activity {

    TextView text1, text2, text3, text4, text5, text6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.key_values_about_cimo);
        init();
    }

    private void init() {
        text1 = findViewById(R.id.serialNumberString);
        text2 = findViewById(R.id.productNameString);
        text3 = findViewById(R.id.productModelString);
        text4 = findViewById(R.id.softVersionString);
        text5 = findViewById(R.id.androidVersionString);
        text6 = findViewById(R.id.brandString);
        setText();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setText() {
        text1.setText(getCimoSn());
        text2.setText(Build.PRODUCT);
        text3.setText(Build.MODEL);
        text4.setText(Build.ID);
        text5.setText(new StringBuffer().append("Android")
                .append(Build.VERSION.RELEASE)
                .append("(API LEVEL ")
                .append(Build.VERSION.SDK_INT)
                .append(")")
        );
        text6.setText(Build.BRAND);
    }

    private String getCimoSn(){
        return Settings.Global.getString(getContentResolver(), "cimo_sn");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}