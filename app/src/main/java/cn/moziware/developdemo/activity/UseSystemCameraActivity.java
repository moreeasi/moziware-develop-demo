
package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.UUID;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;

import androidx.annotation.RequiresApi;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to use the system camera to take pictures and record videos on moziware device.
 */
public class UseSystemCameraActivity extends Activity {
    private static final String TAG = "UseSystemCameraActivity";

    private static final int VIDEO_PLAYBACK_REQUEST_CODE = 5;

    private static final int BITMAP_PHOTO_REQUEST_CODE = 1;
    private static final int FILE_PROVIDER_PHOTO_REQUEST_CODE = 2;
    private static final int BASIC_VIDEO_REQUEST_CODE = 3;
    private static final int FILE_PROVIDER_VIDEO_REQUEST_CODE = 4;

    private static final String DEFAULT_IMAGE_LOCATION = Environment.DIRECTORY_DCIM + "/Camera";
    private static final String DEFAULT_VIDEO_LOCATION = Environment.DIRECTORY_MOVIES + "/Camera";

    private static final String EXTRA_RESULT = "data";

    private static final String DEVELOP_PHOTO_AND_VIDEO_NAME_PRE = "develop-";

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.use_system_camera);

        mImageView = findViewById(R.id.camera_image_view);
    }

    public void onLaunchBitmapPhoto(View view) {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, BITMAP_PHOTO_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void onLaunchFileProviderPhoto(View view) {
        final String fileName = DEVELOP_PHOTO_AND_VIDEO_NAME_PRE + UUID.randomUUID() + ".jpg";

        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, DEFAULT_IMAGE_LOCATION);

        final Uri contentUri = getBaseContext().getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);

        startActivityForResult(captureIntent, FILE_PROVIDER_PHOTO_REQUEST_CODE);
    }

    public void onLaunchBasicVideo(View view) {
        final Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, BASIC_VIDEO_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void onLaunchFileProviderVideo(View view) {
        final String fileName = DEVELOP_PHOTO_AND_VIDEO_NAME_PRE + UUID.randomUUID() + ".mp4";

        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "video/mp4");
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, DEFAULT_VIDEO_LOCATION);

        final Uri contentUri = getBaseContext().getContentResolver().insert(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues);

        final Intent captureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);

        startActivityForResult(captureIntent, FILE_PROVIDER_VIDEO_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case BITMAP_PHOTO_REQUEST_CODE:
                    final Bitmap photo = data.getExtras().getParcelable(EXTRA_RESULT);
                    mImageView.setImageBitmap(photo);
                    break;

                case FILE_PROVIDER_PHOTO_REQUEST_CODE:
                    final Uri photoUri = data.getData();
                    mImageView.setImageURI(photoUri);
                    break;

                case BASIC_VIDEO_REQUEST_CODE:
                case FILE_PROVIDER_VIDEO_REQUEST_CODE:
                    final Uri videoUri = data.getData();

                    final Intent basicVideoPlayIntent = new Intent();
                    basicVideoPlayIntent.setAction(Intent.ACTION_VIEW);
                    basicVideoPlayIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
                    basicVideoPlayIntent.setDataAndType(videoUri, "video/*");
                    startActivityForResult(basicVideoPlayIntent, VIDEO_PLAYBACK_REQUEST_CODE);
                    break;
                default:
                    Log.e(TAG, "Unknown request code: " + requestCode);
                    break;
            }
        }
    }
}
