package cn.moziware.developdemo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import cn.moziware.developdemo.BuildConfig;
import cn.moziware.developdemo.R;
import cn.moziware.developdemo.adaptor.MainMenuTile;
import cn.moziware.developdemo.adaptor.MainMenuTileAdaptor;

public class MainActivity extends AppCompatActivity {

    TextView versionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initGridView();
    }

    private void initGridView(){
        final View[] tileList = new View[]{
                createMenuTile(R.string.camera_command, R.mipmap.camera, "activity.UseSystemCameraActivity"),
                createMenuTile(R.string.document_command, R.mipmap.document, "activity.UseSystemDocumentViewActivity"),
                createMenuTile(R.string.barcode_command, R.mipmap.barcode, "activity.UseSystemBarcodeActivity"),
                createMenuTile(R.string.audio_command, R.mipmap.asr, "activity.HowToRecordSoundActivity"),
                createMenuTile(R.string.movie_command, R.mipmap.movie, "activity.UseSystemMediaPlayerActivity"),
                createMenuTile(R.string.global_commands, R.mipmap.global_commands, "activity.AddNewCommandsActivity"),
                createMenuTile(R.string.key_values, R.mipmap.intent, "activity.KeyValuesAboutCimoActivity"),
        };

        final MainMenuTileAdaptor mainMenuTileAdaptor = new MainMenuTileAdaptor(tileList);
        final GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(mainMenuTileAdaptor);

        //get version
        versionTextView = findViewById(R.id.versionTextView);
        versionTextView.setText(String.format(getResources().getString(R.string.moziware_develop_text4), BuildConfig.VERSION_NAME));

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private MainMenuTile createMenuTile(int voiceCommand, int icon, String activity) {
        return new MainMenuTile(this, voiceCommand, icon, activity);
    }
}
