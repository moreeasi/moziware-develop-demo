package cn.moziware.developdemo.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to do sound recorder at different frequencies on a moziware device.
 */
public class HowToRecordSoundActivity extends Activity implements Runnable {
    private final static String TAG = "HowToRecordSoundActivity";

    private final int SAMPLE_LENGTH_SECONDS = 10;

    private Button mRecordButton;
    private Button mPlaybackButton;
    private ProgressBar mProgressBar;
    CountDownTimer mPlaybackAudioTimer;
    private TextView mFileTextPath;

    private RadioButton mMonoButton;
    private RadioButton mStereoButton;

    private RadioButton mRate8Button;
    private RadioButton mRate16Button;
    private RadioButton mRate44Button;
    private RadioButton mRate48Button;

    private int mSampleRate;
    private String mSampleRatedString;
    private short mChannels;
    private String mChannelsString;

    private AudioRecord mAudioRecorder;

    private Thread mMotor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.how_to_record_sound);

        mRecordButton = findViewById(R.id.recordButton);
        mPlaybackButton = findViewById(R.id.playbackButton);
        mProgressBar = findViewById(R.id.progressBar);
        mFileTextPath = findViewById(R.id.fileTextPath);

        mMonoButton = findViewById(R.id.monoButton);
        mStereoButton = findViewById(R.id.stereoButton);

        mRate8Button = findViewById(R.id.rate8Button);
        mRate16Button = findViewById(R.id.rate16Button);
        mRate44Button = findViewById(R.id.rate44Button);
        mRate48Button = findViewById(R.id.rate48Button);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRecordButton.setEnabled(true);
        mPlaybackButton.setEnabled(true);
        mProgressBar.setVisibility(View.INVISIBLE);
        mFileTextPath.setText("");

        setButtonState(true);
        mRate44Button.performClick();
        mStereoButton.performClick();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mMotor != null) {
            mAudioRecorder.stop();
            mAudioRecorder.release();
            mAudioRecorder = null;

            try {
                mMotor.join();
            } catch (Exception ex) {
                Log.e(TAG, "Failed to stop thread", ex);
            }
        }

        mMotor = null;

        if (mPlaybackAudioTimer != null) {
            mPlaybackAudioTimer.cancel();
        }
    }

    public void onStartRecord(View view) {
        int monoOrStereo = mChannels == 1 ?
                AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO;

        mAudioRecorder = new AudioRecord(
                MediaRecorder.AudioSource.MIC,
                mSampleRate,
                monoOrStereo,
                AudioFormat.ENCODING_PCM_16BIT,
                4096);

        if (mAudioRecorder.getState() == AudioRecord.STATE_UNINITIALIZED) {
            Toast.makeText(this, "Failed to initialize AudioRecorder", Toast.LENGTH_LONG).show();
            return;
        }

        mProgressBar.setProgress(0);
        mProgressBar.setVisibility(View.VISIBLE);
        setButtonState(false);

        mAudioRecorder.startRecording();

        mMotor = new Thread(this);
        mMotor.setName("Recording Thread");
        mMotor.setPriority(Thread.MIN_PRIORITY);
        mMotor.start();
    }

    public void onStartPlayback(View view) {
        final File recordingFile = getRecordingFile();

        try {
            final MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(recordingFile.getAbsolutePath());
            mp.prepare();
            mp.start();

            setButtonState(false);
            mProgressBar.setProgress(0);
            mProgressBar.setVisibility(View.VISIBLE);

            mPlaybackAudioTimer =
                    new CountDownTimer(mp.getDuration(), TimeUnit.SECONDS.toMillis(1)) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            float duration = mp.getDuration();
                            float msPlayed = duration - (int) millisUntilFinished;

                            float progress = msPlayed / duration * mProgressBar.getMax();
                            mProgressBar.setProgress((int) progress);
                        }

                        @Override
                        public void onFinish() {
                            setButtonState(true);
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }.start();
        } catch (IOException ex) {
            Toast.makeText(this, "Failed to play back file", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Playback error: ", ex);
            ex.printStackTrace();
        }
    }

    public void run() {
        int bitsPerSecond = mAudioRecorder.getAudioFormat() == AudioFormat.ENCODING_PCM_16BIT ?
                16 : 8;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096 * 2];

        final long startTime = System.currentTimeMillis();
        float seconds = 0;

        while (seconds < SAMPLE_LENGTH_SECONDS &&
                mAudioRecorder != null &&
                mAudioRecorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
            int bytesRead = mAudioRecorder.read(buffer, 0, buffer.length);
            if (bytesRead > 0) {
                bos.write(buffer, 0, bytesRead);
            }

            seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime);
            updateProgressBar(seconds);
        }

        if (mAudioRecorder != null) {
            mAudioRecorder.stop();
            mAudioRecorder.release();
            mAudioRecorder = null;

            writeWAVFile(getRecordingFile(), bos, bitsPerSecond);
        }

        runOnUiThread(() -> {
            mProgressBar.setVisibility(View.INVISIBLE);
            setButtonState(true);
        });
    }

    private void updateProgressBar(final float seconds) {
        runOnUiThread(() -> {
            float progress = seconds / SAMPLE_LENGTH_SECONDS * mProgressBar.getMax();
            mProgressBar.setProgress((int) progress);
        });
    }

    public void onChannelsChanged(View view) {
        if (view.equals(mMonoButton)) {
            mChannels = 1;
            mChannelsString = "mono";
        }
        if (view.equals(mStereoButton)) {
            mChannels = 2;
            mChannelsString = "stereo";
        }

        updateFileName();
    }

    public void onSampleRateChanged(View view) {
        if (view.equals(mRate8Button)) {
            mSampleRate = 8000;
            mSampleRatedString = "8KHz";
        }

        if (view.equals(mRate16Button)) {
            mSampleRate = 16000;
            mSampleRatedString = "16KHz";
        }
        if (view.equals(mRate44Button)) {
            mSampleRate = 44100;
            mSampleRatedString = "44KHz";
        }
        if (view.equals(mRate48Button)) {
            mSampleRate = 48000;
            mSampleRatedString = "48KHz";
        }

        updateFileName();
    }

    private void updateFileName() {
        mFileTextPath.setText(getRecordingFile().toString());
    }

    private File getRecordingFile() {
        final String filename = "audio_test_" + mSampleRatedString + "_" + mChannelsString + ".wav";
        final File mediaStorageDir = getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        return new File(mediaStorageDir, filename);
    }

    private void setButtonState(boolean isEnabled) {
        mRate8Button.setEnabled(isEnabled);
        mRate16Button.setEnabled(isEnabled);
        mRate44Button.setEnabled(isEnabled);
        mRate48Button.setEnabled(isEnabled);

        mMonoButton.setEnabled(isEnabled);
        mStereoButton.setEnabled(isEnabled);

        mPlaybackButton.setEnabled(isEnabled);
        mRecordButton.setEnabled(isEnabled);
    }

    private void writeWAVFile(File file, ByteArrayOutputStream outputStream, int bitsPerSamples) {
        byte[] audioData = outputStream.toByteArray();

        try {
            FileOutputStream fos = new FileOutputStream(file);
            DataOutputStream dos = new DataOutputStream(fos);

            writeWaveFileHeader(dos, audioData.length, bitsPerSamples);

            fos.write(audioData);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException ex) {
            Toast.makeText(this, "Error creating wav file", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Error creating wav file: ", ex);
        } catch (IOException ex) {
            Toast.makeText(this, "Error writing wav file", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Error writing wav file: ", ex);
        }

        rescanFolder(file.getParent());
    }

    private void writeWaveFileHeader(DataOutputStream dos, int audioLen, int bitsPerSample)
            throws IOException {
        final int wavHeaderSize = 44;
        final int totalDataLen = audioLen + wavHeaderSize - 8 /* Ignore chunk marker and size */;
        final short waveFormatPcm = 0x1;
        final int byteSize = 8;

        //
        // Riff chunk marker and size
        //
        dos.writeBytes("RIFF");
        dos.write(intToByteArray(totalDataLen));
        dos.writeBytes("WAVE");

        //
        // Format chunk marker and size
        //
        dos.writeBytes("fmt ");
        dos.write(intToByteArray(16)); // WAVE file type has 16 bits of format data

        //
        // Format data (16 bits)
        //
        dos.write(shortToByteArray(waveFormatPcm));
        dos.write(shortToByteArray(mChannels));
        dos.write(intToByteArray(mSampleRate));

        int blockAlign = mChannels * bitsPerSample / byteSize;
        int avgBytesPerSec = mSampleRate * blockAlign;

        dos.write(intToByteArray(avgBytesPerSec));
        dos.write(shortToByteArray(Integer.valueOf(blockAlign).shortValue()));
        dos.write(shortToByteArray(Integer.valueOf(bitsPerSample).shortValue()));

        //
        // Data chunk marker and size
        //
        dos.writeBytes("data");
        dos.write(intToByteArray(audioLen));
    }

    private void rescanFolder(String dest) {
        // Scan files only (not folders)
        File[] files = new File(dest).listFiles(File::isFile);

        assert files != null;
        String[] paths = new String[files.length];
        for (int co = 0; co < files.length; co++) {
            paths[co] = files[co].getAbsolutePath();
        }

        MediaScannerConnection.scanFile(this, paths, null, null);

        // and now recursively scan subfolders
        files = new File(dest).listFiles(File::isDirectory);

        assert files != null;
        for (File file : files) {
            rescanFolder(file.getAbsolutePath());
        }
    }

    private static byte[] intToByteArray(int data) {
        return new byte[]{
                (byte) (data & 0xff),
                (byte) ((data >> 8) & 0xff),
                (byte) ((data >> 16) & 0xff),
                (byte) ((data >> 24) & 0xff)
        };
    }

    private static byte[] shortToByteArray(short data) {
        return new byte[]{(byte) (data & 0xff), (byte) ((data >> 8) & 0xff)};
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                setResult(-1);
                Toast.makeText(this, R.string.permissions_error, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}