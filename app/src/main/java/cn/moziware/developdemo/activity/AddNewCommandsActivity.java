package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to add commands to the Global commands list on moziware device
 */
public class AddNewCommandsActivity extends Activity{

    private static final String ACTION_UPDATE_HELP =
            "com.realwear.wearhf.intent.action.UPDATE_HELP_COMMANDS";

    private static final String EXTRA_TEXT = "com.realwear.wearhf.intent.extra.HELP_COMMANDS";
    private static final String EXTRA_SOURCE = "com.realwear.wearhf.intent.extra.SOURCE_PACKAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.add_new_commands_to_global_commands);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ArrayList<String> globalCommands = new ArrayList<>();
        globalCommands.add(getResources().getString(R.string.moziware_develop_text17));
        globalCommands.add(getResources().getString(R.string.moziware_develop_text18));
        globalCommands.add(getResources().getString(R.string.moziware_develop_text19));

        final Intent intent = new Intent(ACTION_UPDATE_HELP);
        intent.putStringArrayListExtra(EXTRA_TEXT, globalCommands);
        intent.putExtra(EXTRA_SOURCE, getPackageName());
        sendBroadcast(intent);
    }

    public void onTriggerBattery(View view){
        ((ImageView) findViewById(R.id.battery)).setImageResource(R.drawable.ic_settings_battery_white);
    }
    public void onTriggerDisplay(View view){
        ((ImageView) findViewById(R.id.display)).setImageResource(R.drawable.ic_settings_display_white);
    }
    public void onTriggerSecure(View view){
        ((ImageView) findViewById(R.id.security)).setImageResource(R.drawable.ic_settings_security_white);
    }
}